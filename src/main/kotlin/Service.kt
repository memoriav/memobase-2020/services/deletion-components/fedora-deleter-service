/*
 * fedora-deleter-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.exceptions.SftpClientException
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.SettingsLoader
import ch.memobase.sftp.SftpClient
import java.io.Closeable
import java.util.Properties
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.fcrepo.client.FcrepoOperationFailedException
import org.memobase.exceptions.MissingMimeTypeException
import org.memobase.fedora.FedoraClient
import org.memobase.fedora.FedoraClientImpl

class Service(fileName: String = "app.yml") : Closeable {

    companion object {
        const val FEDORA_PROPERTIES_PREFIX = "fedora"

        fun createFedoraClient(appSettings: Properties): FedoraClient {
            return FedoraClientImpl.builder()
                .properties(appSettings, FEDORA_PROPERTIES_PREFIX)
                .build()
        }
    }

    private val settings = SettingsLoader(
        listOf(
            "isSimple",
            "$FEDORA_PROPERTIES_PREFIX.internalBaseUrl",
            "$FEDORA_PROPERTIES_PREFIX.externalBaseUrl",
            "$FEDORA_PROPERTIES_PREFIX.username",
            "$FEDORA_PROPERTIES_PREFIX.password"
        ),
        fileName,
        useProducerConfig = true,
        useConsumerConfig = true
    )

    private val log: Logger = LogManager.getLogger("FedoraDeleterService")
    private val fedoraClient = createFedoraClient(settings.appSettings)
    private var consumer: Consumer
    private var producer: Producer

    init {
        val consumerSettings = settings.kafkaConsumerSettings
        consumerSettings.setProperty("max.poll.records", CONSUMER_MAX_POLL_RECORDS)
        consumerSettings.setProperty("max.poll.interval.ms", CONSUMER_MAX_INTERVAL_MS)
        consumer = Consumer(consumerSettings, settings.inputTopic)
        producer = Producer(settings.kafkaProducerSettings, settings.processReportTopic)
        log.info("Connected to Kafka cluster.")
    }

    fun run() {
        while (true) {
            processRecords()
        }
    }

    fun processRecords() {
        // TODO
    }

    private fun processRecord(record: ConsumerRecord<String, String>): Report {
        // TODO
    }

    override fun close() {
        consumer.close()
        producer.close()
    }
}
