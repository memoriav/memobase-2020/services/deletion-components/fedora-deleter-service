# Fedora Deleter

Reads delete messages from a Kafka topic (created by "Import Process Delete") and deletes the corresponding Fedora resources.